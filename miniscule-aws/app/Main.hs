import Data.String.Conv (toS)
import Data.Config (defaultResolver, decode)
import Data.YAML (Node (..), encode1, Pos)
import Data.Config.AWS (awsSecretResolver)

main :: IO ()
main = do
  contents <- getContents
  node <- decode resolver (toS contents)
  putStr $ toS $ encode1 (node :: Node Pos)

  where
    resolver = awsSecretResolver <> defaultResolver
