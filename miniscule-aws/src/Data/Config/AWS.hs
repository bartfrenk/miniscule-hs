module Data.Config.AWS where

import           Control.Monad.Except
import           Control.Monad.Trans (MonadIO)
import           Control.Monad.Trans.AWS
import           Data.Config.Internals
import           Data.Text (Text)
import           Data.YAML (Node (..), Scalar (..))
import           Data.YAML.Event (mkTag)
import           Lens.Micro
import           Network.AWS.Env (newEnv)
import           Network.AWS.SecretsManager.GetSecretValue


awsSecretResolver :: MonadIO m => Resolver m
awsSecretResolver = Resolver awsSecretResolver'
  where
    awsSecretResolver' cont (Scalar _ (SUnknown tag text)) =
      if | tag == mkTag "!aws/secret" -> Just $
             liftIO (lookupSecret text) >>= \case
               Nothing -> throwError "Could not find secret"
               Just s -> decodeNode s >>= cont -- Here we need to be more subtle, as JSON may be returned
         | otherwise -> Nothing
    awsSecretResolver' _ _ = Nothing


getRegion :: IO Region
getRegion = pure Ireland

lookupSecret :: Text -> IO (Maybe Text)
lookupSecret text = do
  env <- newEnv Discover
  region <- getRegion
  resp <- runResourceT $ runAWST env $ within region $
    send $ getSecretValue text
  pure $ resp ^. gsvrsSecretString


