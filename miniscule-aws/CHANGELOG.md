# Changelog

## [Unreleased]

### Added

- Added resolver for !aws/secret that looks up the string in AWS secrets
  manager.
