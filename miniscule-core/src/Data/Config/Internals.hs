module Data.Config.Internals where

import           Control.Monad.Catch (Exception, MonadThrow, throwM)
import           Control.Monad.Except
import           Control.Monad.Trans
import           Control.Monad.Trans.Maybe
import           Data.ByteString.Lazy (ByteString, readFile)
import           Data.Either
import           Data.Maybe
import           Data.Monoid
import           Data.String.Conv
import           Data.YAML (Doc (..), FromYAML, Node (..), Scalar (..))
import qualified Data.YAML as YAML
import           Data.YAML.Event (Pos, Tag, mkTag)
import           Prelude hiding (readFile)
import           System.Environment (lookupEnv)

type Node' = YAML.Node Pos

newtype ParseError = ParseError String deriving (Eq, Show)

instance Exception ParseError

-- |Load YAML from file.
load :: (MonadIO m, MonadThrow m, FromYAML v) => Resolver m -> FilePath -> m v
load res path = liftIO (readFile path) >>= decode res

-- |Decode bytestring as YAML.
decode :: (MonadIO m, MonadThrow m, FromYAML v)
       => Resolver m -> ByteString -> m v
decode res bs = fromErrT (decodeNode bs >>= resolve res >>= fromNode)
  where
    fromErrT act = runExceptT act >>= \case
      Left err -> throwM $ ParseError err
      Right v -> pure v

-- |Resolver that accepts !env, !or and default tags.
defaultResolver :: MonadIO m => Resolver m
defaultResolver = orResolver <> envResolver <> baseResolver

type ErrT m a = ExceptT String m a

newtype Resolver m =
  Resolver ((Node' -> ErrT m Node') -> Node' -> Maybe (ErrT m Node'))

instance Semigroup (Resolver m) where
  (Resolver f) <> (Resolver g) = Resolver $ \cont node ->
    case f cont node of
      Just n  -> Just n
      Nothing -> g cont node

decodeNode :: (Monad m, StringConv s ByteString) => s -> ErrT m Node'
decodeNode s = case YAML.decodeNode (toS s) of
  Left (pos, err)  -> throwError $ err <> "(" <> show pos <> ")"
  Right [Doc node] -> pure node
  _                -> throwError "Multiple YAML not supported"

fromNode :: (Monad m, FromYAML v) => Node' -> ErrT m v
fromNode node = case YAML.parseEither $ YAML.parseYAML node of
  Left (pos, err) -> throwError $ err <> "(" <> show pos <> ")"
  Right v         -> pure v

-- |Resolver that accepts !or tags.
orResolver :: Monad m => Resolver m
orResolver = Resolver orResolver'
  where

    orResolver' cont node@(Sequence _ tag nodes) =
      if | tag == mkTag "!or" -> Just . ExceptT $ do
             results <- (runExceptT . cont) `mapM` nodes
             case firstSuccess results of
               Nothing -> pure $ Left "No successful resolver for !or tag"
               Just a  -> pure $ Right a
         | otherwise -> Nothing
    orResolver' _ _ = Nothing

    firstSuccess xs = case dropWhile isLeft xs of
      []          -> Nothing
      (Right x:_) -> Just x

-- |Resolver that accepts !env tags.
envResolver :: MonadIO m => Resolver m
envResolver = Resolver envResolver'
  where
    envResolver' cont (Scalar _ (SUnknown tag text)) =
      if | tag == mkTag "!env" -> Just $
             liftIO (lookupEnv (toS text)) >>= \case
               Nothing -> throwError "Could not find env"
               Just s -> decodeNode s >>= cont
         | otherwise -> Nothing
    envResolver' _ _ = Nothing

-- |Resolver that accepts regular YAML tags.
baseResolver :: Monad m => Resolver m
baseResolver = Resolver baseResolver'
  where

    baseResolver' _ node@(Scalar _ _) = Just (pure node)
    baseResolver' cont (Mapping pos tag mapping) =
      Just (Mapping pos tag <$> cont `mapM` mapping)
    baseResolver' cont (Sequence pos tag nodes) =
      Just (Sequence pos tag <$> cont `mapM` nodes)
    baseResolver' _ node = Just (pure node)

resolve :: MonadThrow m => Resolver m -> Node' -> ErrT m Node'
resolve res@(Resolver f) node = fromMaybe err (f (resolve res) node)
  where
    err = throwError "Failed to resolve"
