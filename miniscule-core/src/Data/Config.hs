module Data.Config
  ( load
  , decode
  , defaultResolver
  , envResolver
  , baseResolver
  , orResolver
  , ParseError
  ) where

import           Data.Config.Internals

