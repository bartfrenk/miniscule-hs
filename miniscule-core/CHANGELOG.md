# Changelog

## [Unreleased]

### Added

- Added resolver for !env tags.
- Added resolver for !or tags.

### Changed

- Updated the dependency on HsYAML from 0.1.1.3 to 0.2.1.0.
