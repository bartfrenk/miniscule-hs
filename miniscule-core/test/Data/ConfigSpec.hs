module Data.ConfigSpec where

import           Control.Exception
import qualified Data.Map.Strict    as Map
import           Data.Text
import           System.Environment
import           Test.Hspec

import           Data.Config

isParseError :: ParseError -> Bool
isParseError = const True

spec :: Spec
spec = do
  describe "orResolver" $ do
    it "resolves to the first literal" $
      let yaml = "!or [1, 2, 3]"
          resolver = orResolver <> baseResolver
      in decode resolver yaml `shouldReturn` (1 :: Int)
    it "skips unresolvable items in list" $
      let yaml = "!or [!env BOGUS, 2, 3]"
          resolver = orResolver <> envResolver <> baseResolver
      in decode resolver yaml `shouldReturn` (2 :: Int)
    it "fails when all items are unresolvable" $
      let yaml = "!or [!env BOGUS, !env BOGUS]"
          resolver = orResolver <> envResolver <> baseResolver
      in (decode resolver yaml :: IO Int) `shouldThrow` isParseError

  describe "envResolver" $ do
    it "fails when the environment variable is not defined" $
      let yaml = "!env BOGUS"
          resolver = envResolver
      in (decode resolver yaml :: IO Int) `shouldThrow` isParseError
    it "resolves to the value of the environment variable" $
      let yaml = "!env HOST"
          resolver = envResolver <> baseResolver
          action = withEnv "HOST" "value" (decode resolver yaml)
      in action `shouldReturn` ("value" :: Text)
    it "parses the value of the variable as YAML" $
      let yaml = "!env HOST"
          resolver = envResolver <> baseResolver
          action = withEnv "HOST" "key: value" (decode resolver yaml)
      in action `shouldReturn` Map.fromList [("key" :: Text, "value" :: Text)]
    it "applies other resolvers to the value of the variable" $
      let yaml = "!env HOST"
          resolver = envResolver <> orResolver <> baseResolver
          action = withEnv "HOST" "!or [!env BOGUS, 2]" (decode resolver yaml)
      in action `shouldReturn` (2 :: Int)

  describe "baseResolver" $ do
    it "fails on unknown tags" $
      let yaml = "!env HOST"
      in (decode baseResolver yaml :: IO Text) `shouldThrow` isParseError
    it "parses untagged yaml" $
      let yaml = "value"
      in decode baseResolver yaml `shouldReturn` ("value" :: Text)

-- |Convenience function to run an action with an environment variable set.
withEnv :: String -> String -> IO a -> IO a
withEnv name value = bracket_ (setEnv name value) (unsetEnv name)
