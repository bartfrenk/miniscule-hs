# TODO

In priority order:

* Dynamically determine region (by AWS_REGION, AWS_DEFAULT_REGION)
* Add builder to speed up the CICD pipeline.
* Allow deriving `FromYAML` instances; this would preferably go into HsYAML. (or
  just use HsYAML-aeson to parse YAML from FromJSON instances, although it seems
  that the tags would be lost after conversion to `Value`. That would make that
  route impossible. However, having a function `Node -> Either String Value`
  would suffice.)
* Write documentation and a better README.
